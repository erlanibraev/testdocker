package kz.factor.tofi.synchronizer.docker.test.repository.entities;

import javax.persistence.*;

/**
 * Created by Erlan.Ibraev on 01.08.2016.
 */
@Entity
@Table(name="MetaModelCod", uniqueConstraints = {@UniqueConstraint(columnNames = {"codSS", "codMM", "codEx"})})
public class MetaModelCodEntity {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="metamodelcod_id_seq")
    private Long id;
    @Column(name="codSS", length = 1000, nullable = false)
    private String codSS;
    @Column(name="codMM", length = 1000, nullable = false)
    private String codMM;
    @Column(name="codEx", length = 1000, nullable = false)
    private String codEx;
    @Column(name="entityType", length=1000)
    private String entityType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodSS() {
        return codSS;
    }

    public void setCodSS(String codSS) {
        this.codSS = codSS;
    }

    public String getCodMM() {
        return codMM;
    }

    public void setCodMM(String codMM) {
        this.codMM = codMM;
    }

    public String getCodEx() {
        return codEx;
    }

    public void setCodEx(String codEx) {
        this.codEx = codEx;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }
}
