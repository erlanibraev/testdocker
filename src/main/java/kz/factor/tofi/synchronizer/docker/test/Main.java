package kz.factor.tofi.synchronizer.docker.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by mad on 21.09.16.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = "kz.factor.tofi.synchronizer.docker.test.repository")
@EntityScan(basePackages = {"kz.factor.tofi.synchronizer.docker.test.repository.entities"})
public class Main {

    public static void main(String... args) {
        SpringApplication.run(Main.class, args);
    }
}
