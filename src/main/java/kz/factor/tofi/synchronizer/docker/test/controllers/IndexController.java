package kz.factor.tofi.synchronizer.docker.test.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mad on 21.09.16.
 */
@RestController
@RequestMapping(value="/")
public class IndexController {

    @RequestMapping(value = ""
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getIndex() {
        return "ТЕСТ";
    }
}
