package kz.factor.tofi.synchronizer.docker.test.repository;

import kz.factor.tofi.synchronizer.docker.test.repository.entities.MetaModelCodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by mad on 21.09.16.
 */
@Repository
public interface IMteaModelCodRepository extends JpaRepository<MetaModelCodEntity, Long>, JpaSpecificationExecutor<MetaModelCodEntity> {

}
